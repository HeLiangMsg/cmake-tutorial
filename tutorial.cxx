#include <iostream>
#include "TutorialConfig.h"

#ifdef USE_MYMATH
#include "MathFunctions/MathFunctions.h"
#else
#include <cmath>
#endif

int main(int argc, char *argv[])
{
    std::cout << argv[0] << " Version "
              << Tutorial_VERSION_MAJOR << "."
              << Tutorial_VERSION_MINOR << std::endl;
    std::cout << "Usage: " << argv[0] << " number" << std::endl;

#ifdef USE_MYMATH
std::cout << "InvSqrt" << '\n';
#else
std::cout << "sqrt" << '\n';
#endif
    if (argc >= 2)
    {
        const double inputValue = std::stod(argv[1]);

#ifdef USE_MYMATH
        const double outputValue = InvSqrt(inputValue);
#else
        const double outputValue = sqrt(inputValue);
#endif
        std::cout << outputValue << std::endl;
    }
}